using MartianRobots.Actions.Actions.Commands;
using MartianRobots.Core.Actions.Commands;

namespace MartianRobotsTests;
[TestFixture]
public class RobotCommandActionFactoryTests
{
    private static IEnumerable<TestCaseData> RobotCommandActionFactoryTestsCases
    {
        get
        {
            yield return new TestCaseData(RobotCommands.ReleaseOntoPlanet, typeof(ReleaseARobotOntoAPlanetAction));
            yield return new TestCaseData(RobotCommands.Forward, typeof(MoveRobotForwardAction));
            yield return new TestCaseData(RobotCommands.Left, typeof(RotateRobotLeftAction));
            yield return new TestCaseData(RobotCommands.Right, typeof(RotateRobotRightAction));
        }
    }


    [Test]
    public void ARobotCommandActionFactoryCanBeCreated()
    {
        new RobotCommandActionFactory(null,null);
    }

    [TestCaseSource(nameof(RobotCommandActionFactoryTestsCases))]
    public void ARobotCommandActionForARobotCommandCanBeCreated(RobotCommands command, Type type)
    {
        var factory = new RobotCommandActionFactory(null, null);
        var action = factory.Create(command);

        Assert.That(action.GetType(), Is.EqualTo(type));
    }

}