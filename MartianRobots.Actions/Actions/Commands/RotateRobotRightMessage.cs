namespace MartianRobots.Actions.Actions.Commands;
public class RotateRobotRightMessage : RobotCommandActionMessage
{
    public RotateRobotRightMessage(Robot.Robot robot) : base(robot)
    {
    }
}