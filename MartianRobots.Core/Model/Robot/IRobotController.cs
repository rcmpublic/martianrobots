using System;
using System.ComponentModel.Design.Serialization;

namespace MartianRobots.Core.Model.Robot;
public interface IRobotController
{
    public Type StateType { get; }

    IRobotState ReadyToReleaseState { get; }
    IRobotState EastState { get; }
    IRobotState NorthState { get; }
    IRobotState WestState { get; }
    IRobotState SouthState { get; }
    IRobotState LostState { get; }
    void RotateLeft();

    void RotateRight();

    void MoveRobotForward();

    void ReleaseOntoPlanet(Mars mars);

    void SetState(IRobotState state);

}
