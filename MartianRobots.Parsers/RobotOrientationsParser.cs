using System;
using System.Text.RegularExpressions;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class RobotOrientationsParser : IRobotOrientationsParser
{
    public string Pattern => @$"([NSWE])";
    public RobotOrientations Parse(string input)
    {
        Match match = Regex.Match(input, Pattern);
        string o = match.Groups[1].Value;

        RobotOrientations orientation;
        if (o == "N")
            orientation = RobotOrientations.North;
        else if (o == "S")
            orientation = RobotOrientations.South;
        else if (o == "E")
            orientation = RobotOrientations.East;
        else if (o == "W")
            orientation = RobotOrientations.West;
        else
            throw new InvalidOperationException($"Invalid orientation {o}");

        return orientation;

    }
}
