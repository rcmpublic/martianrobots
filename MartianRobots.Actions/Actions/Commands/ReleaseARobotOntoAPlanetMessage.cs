using MartianRobots.Core.Model;

namespace MartianRobots.Actions.Actions.Commands;
public class ReleaseARobotOntoAPlanetMessage : RobotCommandActionMessage
{
    public Mars Planet { get; private set; }
    public ReleaseARobotOntoAPlanetMessage(Robot.Robot robot, Mars planet) : base(robot)
    {
        Planet = planet;
    }
}