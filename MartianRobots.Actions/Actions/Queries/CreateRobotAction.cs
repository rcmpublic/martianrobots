namespace MartianRobots.Actions.Actions.Queries;

public class CreateRobotAction
{
    private CreateRobotMessage Message { get; set; }

    public CreateRobotAction(CreateRobotMessage message)
    {
        Message = message;
    }

    public Robot.Robot Execute()
    {
        var robot = new Robot.Robot(Message.Vector.Location, Message.Vector.Orientation);
        robot.RobotController = new Robot.RobotController(robot);
        return robot;
    }
}