using System.IO;
using System.Collections.Generic;
using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Model;

namespace MartianRobots.Core.Parsers;

public interface ISpaceProgramParser
{
    public bool EndReached { get; }
    public Dimension ParsePlanetDimension();
    public Vector ParseRobotVector();
    public IEnumerable<RobotCommands> ParseRobotCommands();

}
