namespace MartianRobots.Core.Actions.Commands;
public class RobotCommands
{
    public static RobotCommands ReleaseOntoPlanet = new RobotCommands(nameof(ReleaseOntoPlanet));
    public static RobotCommands Forward = new RobotCommands(nameof(Forward));
    public static RobotCommands Left = new RobotCommands(nameof(Left));
    public static RobotCommands Right = new RobotCommands(nameof(Right));
    public static RobotCommands West = new RobotCommands(nameof(West));

    public string Command { get; private set; }

    public RobotCommands(string command)
    {
        Command = command;
    }
    public override string ToString()
    {
        return Command;
    }
}