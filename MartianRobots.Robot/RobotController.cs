using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Robot.RobotStates;
using System;

namespace MartianRobots.Robot;
public class RobotController : IRobotController
{
    private IRobotState _currentState;
    private Location _previousLocation = Location.NullLocation;
    public Mars Mars { get; private set; }

    public Robot Robot { get; private set; }
    public Type StateType { get { return _currentState.GetType(); } }
    public bool Lost { get { return _currentState == LostState || Mars != null && !Mars.Contains(Robot.Location); } }

    public IRobotState ReadyToReleaseState { get; private set; }
    public IRobotState EastState { get; private set; }
    public IRobotState NorthState { get; private set; }
    public IRobotState WestState { get; private set; }
    public IRobotState SouthState { get; private set; }
    public IRobotState LostState { get; private set; }
    public RobotController(Robot robot)
    {
        Robot = robot;

        ReadyToReleaseState = new ReadyToReleaseState(this);
        EastState = new EastState(this);
        NorthState = new NorthState(this);
        WestState = new WestState(this);
        SouthState = new SouthState(this);
        LostState = new LostState(this);

        _currentState = ReadyToReleaseState;
    }

    public void RotateLeft()
    {
        _currentState.RotateLeft();
    }

    public void RotateRight()
    {
        _currentState.RotateRight();
    }

    public void MoveRobotForward()
    {
        _previousLocation = Robot.Location;
        Robot.Location = Robot.Location.Offset(_currentState.ForwardOffset);
        Robot.Lost = Lost;
        if (Robot.Lost)
        {
            SetState(LostState);
            Robot.Location = _previousLocation;
        }
    }

    public void ReleaseOntoPlanet(Mars mars)
    {
        Mars = mars;

        if (Robot.Orientation == RobotOrientations.North)
            SetState(NorthState);
        else if (Robot.Orientation == RobotOrientations.South)
            SetState(SouthState);
        else if (Robot.Orientation == RobotOrientations.East)
            SetState(EastState);
        else if (Robot.Orientation == RobotOrientations.West)
            SetState(WestState);
        else
            throw new NotImplementedException($"Robot state for orientation '{Robot.Orientation}' not implemented");

        Robot.Orientation = _currentState.Orientation;

        if (Lost)
        {
            SetState(LostState);
        }
    }

    public void SetState(IRobotState state)
    {
        _currentState = state;
        if (!Lost)
            Robot.Orientation = state.Orientation;
    }

}
