using System.Text.RegularExpressions;
using MartianRobots.Core.Model;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class DimensionParser : IDimensionParser
{
    private const string OneOrMoreDigits = @"(\d+)";
    private const string OneBlankSpace = @"\s";
    public static string Pattern => @$"{OneOrMoreDigits}{OneBlankSpace}{OneOrMoreDigits}";
    public Dimension Parse(string input)
    {
        Match match = Regex.Match(input, Pattern);

        int sizeX = int.Parse(match.Groups[1].Value);
        int sizeY = int.Parse(match.Groups[2].Value);

        return new Dimension() { SizeX = sizeX, SizeY = sizeY };

    }
}
