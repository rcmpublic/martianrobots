using MartianRobots.Application;
using MartianRobots.IO;
using MartianRobots.Parsers;
using MartianRobots.Persistence;

namespace MartianRobotsTests;

[TestFixture]
public class SpaceProgramExecutorTests
{
    private static IEnumerable<TestCaseData> SpaceProgramExecutorCanWriteProgramOutputCases
    {
        get
        {
            var spaceProgram1 = @"5 3
1 1 E
RFRFRFRF
";

            var expectedOutput1 = @"1 1 E
";

            yield return new TestCaseData(spaceProgram1, expectedOutput1);

            var spaceProgram2 = @"5 3
1 1 E
RFRFRFRF
3 2 N
FRRFLLFFRRFLL
";

            var expectedOutput2 = @"1 1 E
3 3 N LOST
";

            yield return new TestCaseData(spaceProgram2, expectedOutput2);

            var spaceProgram3 = @"5 3
1 1 E
RFRFRFRF
3 2 N
FRRFLLFFRRFLL
0 3 W
LLFFFRFLFL
";

            var expectedOutput3 = @"1 1 E
3 3 N LOST
4 2 N
";
            yield return new TestCaseData(spaceProgram3, expectedOutput3);

        }
    }


    private static IEnumerable<TestCaseData> AnExecutorWillNotAllowRoboToBeLostAtScentsLocationsCases
    {
        get
        {

            var spaceProgram1 = @"0 0
0 0 E
FFFFF
0 0 E
FFFFFFF
";

            var expectedOutput1 = @"0 0 E LOST
0 0 E
";

            yield return new TestCaseData(spaceProgram1, expectedOutput1);

            var spaceProgram2 = @"2 2
1 1 N
FFFFF
1 1 N   
FFFRRFLL
";

            var expectedOutput2 = @"1 2 N LOST
1 1 N
";

            yield return new TestCaseData(spaceProgram2, expectedOutput2);

            var spaceProgram3 = @"0 0
0 0 N
F
0 0 S
F
0 0 E
F
0 0 W
F
0 0 N
F
0 0 S
F
0 0 E
F
0 0 W
F
";

            var expectedOutput3 = @"0 0 N LOST
0 0 S LOST
0 0 E LOST
0 0 W LOST
0 0 N
0 0 S
0 0 E
0 0 W
";

            yield return new TestCaseData(spaceProgram3, expectedOutput3);

        }
    }



    [Test]
    public void ASpaceProgramExecutorCanBeCreated()
    {
        var spaceProgramParser = CreateSpaceProgramParser("");
        var spaceProgramWriter = new SpaceProgramOutputWriter(new StringWriter());
        var scentsRepository = new ScentsRepository();
        new SpaceProgramExecutor(spaceProgramParser, spaceProgramWriter, scentsRepository);
    }

    [Test]
    public void ASpaceProgramExecutorCanExecuteAProgram()
    {
        var spaceProgram = @"5 3
1 1 E
RFRFRFRF
";
        var spaceProgramParser = CreateSpaceProgramParser(spaceProgram);
        var spaceProgramWriter = new SpaceProgramOutputWriter(new StringWriter());
        var scentsRepository = new ScentsRepository();
        new SpaceProgramExecutor(spaceProgramParser, spaceProgramWriter, scentsRepository).Execute();
    }

    [TestCaseSource(nameof(SpaceProgramExecutorCanWriteProgramOutputCases))]
    public void ASpaceProgramExecutorCanWriteProgramOutput(string spaceProgram, string expectedOutput)
    {
        var writer = new StringWriter();
        var spaceProgramParser = CreateSpaceProgramParser(spaceProgram);
        var spaceProgramWriter = new SpaceProgramOutputWriter(writer);
        var scentsRepository = new ScentsRepository();
        new SpaceProgramExecutor(spaceProgramParser, spaceProgramWriter, scentsRepository).Execute();
        var actualOutput = writer.ToString();

        Assert.That(actualOutput, Is.EqualTo(expectedOutput));
    }
    [TestCaseSource(nameof(AnExecutorWillNotAllowRoboToBeLostAtScentsLocationsCases))]
    public void AnExecutorWillNotAllowRoboToBeLostAtScentsLocations(string spaceProgram, string expectedOutput)
    {
        var writer = new StringWriter();
        var spaceProgramParser = CreateSpaceProgramParser(spaceProgram);
        var spaceProgramWriter = new SpaceProgramOutputWriter(writer);
        var scentsRepository = new ScentsRepository();
        new SpaceProgramExecutor(spaceProgramParser, spaceProgramWriter, scentsRepository).Execute();
        var actualOutput = writer.ToString();

        Assert.That(actualOutput, Is.EqualTo(expectedOutput));
    }


    private SpaceProgramParser CreateSpaceProgramParser(string spaceProgram)
    {
        var spaceProgramReader = new StringReader(spaceProgram);
        var robotVectorParser = new RobotVectorParser(new LocationParser(), new RobotOrientationsParser());
        var dimensionParser = new DimensionParser();
        var robotCommandsSequenceParser = new RobotCommandsSequenceParser(new RobotCommandsParser());
        return new SpaceProgramParser(spaceProgramReader, dimensionParser, robotVectorParser, robotCommandsSequenceParser);
    }
}
