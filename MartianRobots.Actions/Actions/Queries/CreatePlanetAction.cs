using MartianRobots.Core.Model;

namespace MartianRobots.Actions.Actions.Queries;

public class CreatePlanetAction
{
    private CreatePlanetMessage Message { get; set; }

    public CreatePlanetAction(CreatePlanetMessage message)
    {
        Message = message;
    }
    public Mars Execute()
    {
        return new Mars(Message.Dimension);
    }
}

