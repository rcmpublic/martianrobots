using System.Text.RegularExpressions;
using MartianRobots.Core.Model;

namespace MartianRobots.Core.Parsers;

public interface IMarsParser
{
    public Mars Parse(string input);
}
