using MartianRobots.Actions.Actions.Commands;
using MartianRobots.Actions.Actions.Queries;
using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Persistence;
using MartianRobots.Robot.RobotStates;

namespace MartianRobotsTests;

[TestFixture]
public class ActionsTests
{
    private ScentsRepository _scentsRepository;
    private static Location OffLocation { get { return new() { X = -1, Y = -1 }; } }
    private static Location OneLocation { get { return new() { X = +1, Y = +1 }; } }
    private static Location ZeroLocation { get { return new() { X = 0, Y = 0 }; } }
    private static Dimension ZeroDimension { get { return new() { SizeX = 0, SizeY = 0 }; } }
    private static Dimension StandardDimension { get { return new() { SizeX = 5, SizeY = 3 }; } }

    private static IEnumerable<TestCaseData> CreateAPlanetUseCases
    {
        get
        {
            yield return new TestCaseData(ZeroDimension).SetName($"{nameof(AUserCanCreateAPlanet)}({nameof(ZeroDimension)})");
            yield return new TestCaseData(StandardDimension).SetName($"{nameof(AUserCanCreateAPlanet)}({nameof(StandardDimension)})"); ;
        }
    }
    private static IEnumerable<TestCaseData> CreateARobotAtAPlanetUseCases
    {
        get
        {
            int n = 0;
            Func<Dimension, Location, RobotOrientations, Type, TestCaseData> CreateARobotAtAPlanetUseCase = (d, l, o, t) =>
                new TestCaseData(d, l, o, t).SetName("{m}" + $"(\"Test {++n}: Create Robot At {l} Planet(Dim {d}) with {t.Name} \")");

            yield return CreateARobotAtAPlanetUseCase(ZeroDimension, ZeroLocation, RobotOrientations.East, typeof(EastState));
            yield return CreateARobotAtAPlanetUseCase(ZeroDimension, ZeroLocation, RobotOrientations.North, typeof(NorthState));
            yield return CreateARobotAtAPlanetUseCase(ZeroDimension, ZeroLocation, RobotOrientations.West, typeof(WestState));
            yield return CreateARobotAtAPlanetUseCase(ZeroDimension, ZeroLocation, RobotOrientations.South, typeof(SouthState));
            yield return CreateARobotAtAPlanetUseCase(ZeroDimension, new Location() { X = 1, Y = 0 }, RobotOrientations.South, typeof(LostState));
        }
    }

    private static IEnumerable<TestCaseData> ClockWiseRotationUseCases
    {
        get
        {
            int n = 0;
            Func<Location, RobotOrientations, RobotOrientations, Type, TestCaseData> ClockWiseRotationUseCase = (l, io, eo, t) =>
                new TestCaseData(l, io, eo, t).SetName("{m}" + $"(\"Test {++n}: Robot Clockwise rotated from {io} to {eo} will be in {t.Name} \")");

            yield return ClockWiseRotationUseCase(OneLocation, RobotOrientations.North, RobotOrientations.East, typeof(EastState));
            yield return ClockWiseRotationUseCase(OneLocation, RobotOrientations.East, RobotOrientations.South, typeof(SouthState));
            yield return ClockWiseRotationUseCase(OneLocation, RobotOrientations.South, RobotOrientations.West, typeof(WestState));
            yield return ClockWiseRotationUseCase(OneLocation, RobotOrientations.West, RobotOrientations.North, typeof(NorthState));

            yield return ClockWiseRotationUseCase(OffLocation, RobotOrientations.North, RobotOrientations.North, typeof(LostState));
            yield return ClockWiseRotationUseCase(OffLocation, RobotOrientations.East, RobotOrientations.East, typeof(LostState));
            yield return ClockWiseRotationUseCase(OffLocation, RobotOrientations.South, RobotOrientations.South, typeof(LostState));
            yield return ClockWiseRotationUseCase(OffLocation, RobotOrientations.West, RobotOrientations.West, typeof(LostState));
        }
    }

    private static IEnumerable<TestCaseData> CounterClockwiseRotationUseCases
    {
        get
        {
            int n = 0;
            Func<Location, RobotOrientations, RobotOrientations, Type, TestCaseData> CounterClockwiseRotationUseCase = (l, io, eo, t) =>
                new TestCaseData(l, io, eo, t).SetName("{m}" + $"(\"Test {++n}: Robot CounterClockwise rotated from {io} to {eo} will be in {t.Name} \")");

            yield return CounterClockwiseRotationUseCase(OneLocation, RobotOrientations.North, RobotOrientations.West, typeof(WestState));
            yield return CounterClockwiseRotationUseCase(OneLocation, RobotOrientations.West, RobotOrientations.South, typeof(SouthState));
            yield return CounterClockwiseRotationUseCase(OneLocation, RobotOrientations.South, RobotOrientations.East, typeof(EastState));
            yield return CounterClockwiseRotationUseCase(OneLocation, RobotOrientations.East, RobotOrientations.North, typeof(NorthState));

            yield return CounterClockwiseRotationUseCase(OffLocation, RobotOrientations.North, RobotOrientations.North, typeof(LostState));
            yield return CounterClockwiseRotationUseCase(OffLocation, RobotOrientations.West, RobotOrientations.West, typeof(LostState));
            yield return CounterClockwiseRotationUseCase(OffLocation, RobotOrientations.South, RobotOrientations.South, typeof(LostState));
            yield return CounterClockwiseRotationUseCase(OffLocation, RobotOrientations.East, RobotOrientations.East, typeof(LostState));
        }
    }

    private static IEnumerable<TestCaseData> ForwardMovementsUseCases
    {
        get
        {
            int n = 0;
            Func<Location, RobotOrientations, Location, Type, TestCaseData> ForwardMovementsUseCase = (i, o, e, t) =>
                new TestCaseData(i, o, e, t).SetName("{m}" + $"(\"Test {++n}: FW At {o} From {i} To {e} and state {t.Name} \")");

            yield return ForwardMovementsUseCase(OneLocation, RobotOrientations.North, new Location() { X = 1, Y = 2 }, typeof(NorthState));
            yield return ForwardMovementsUseCase(OneLocation, RobotOrientations.South, new Location() { X = 1, Y = 0 }, typeof(SouthState));
            yield return ForwardMovementsUseCase(OneLocation, RobotOrientations.East, new Location() { X = 2, Y = 1 }, typeof(EastState));
            yield return ForwardMovementsUseCase(OneLocation, RobotOrientations.West, new Location() { X = 0, Y = 1 }, typeof(WestState));
            yield return ForwardMovementsUseCase(OffLocation, RobotOrientations.North, OffLocation, typeof(LostState));
            yield return ForwardMovementsUseCase(OffLocation, RobotOrientations.South, OffLocation, typeof(LostState));
            yield return ForwardMovementsUseCase(OffLocation, RobotOrientations.East, OffLocation, typeof(LostState));
            yield return ForwardMovementsUseCase(OffLocation, RobotOrientations.West, OffLocation, typeof(LostState));
        }
    }

    private static IEnumerable<TestCaseData> MoveARobotForwardOffAndEdgeUseCases
    {
        get
        {
            yield return new TestCaseData(RobotOrientations.North);
            yield return new TestCaseData(RobotOrientations.South);
            yield return new TestCaseData(RobotOrientations.East);
            yield return new TestCaseData(RobotOrientations.West);
        }
    }

    [SetUp]
    public void SetUp()
    {
        _scentsRepository = new ScentsRepository();
    }

    [TestCaseSource(nameof(CreateAPlanetUseCases))]
    public void AUserCanCreateAPlanet(Dimension dimension)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(dimension)).Execute();

        Assert.That(planet.Dimension, Is.EqualTo(dimension));
    }

    [TestCaseSource(nameof(CreateARobotAtAPlanetUseCases))]
    public void AUserCanCreateARobotAtAPlanet(Dimension dimension, Location location, RobotOrientations orientation, Type stateType)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(dimension)).Execute();

        var robot = new CreateRobotAction(new CreateRobotMessage(new Vector(location, orientation))).Execute();
        new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(robot, planet)).Execute();


        Assert.Multiple(() =>
        {
            Assert.That(robot.Location, Is.EqualTo(location));
            Assert.That(robot.Orientation, Is.EqualTo(orientation));
            Assert.That(robot.RobotController.StateType, Is.EqualTo(stateType));
        });
    }

    [TestCaseSource(nameof(ClockWiseRotationUseCases))]
    public void AUserCanRotateARobotClockWise(Location location, RobotOrientations initialOrientation, RobotOrientations endOrientaion, Type stateType)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(StandardDimension)).Execute();
        var robot = new CreateRobotAction(new CreateRobotMessage(new Vector(location, initialOrientation))).Execute();
        new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(robot, planet)).Execute();

        new RotateRobotRightAction(new RotateRobotRightMessage(robot)).Execute();

        Assert.Multiple(() =>
        {
            Assert.That(robot.Location, Is.EqualTo(location));
            Assert.That(robot.Orientation, Is.EqualTo(endOrientaion));
            Assert.That(robot.RobotController.StateType, Is.EqualTo(stateType));
        });
    }

    [TestCaseSource(nameof(CounterClockwiseRotationUseCases))]
    public void AUserCanRotateARobotCounterClockwise(Location location, RobotOrientations initialOrientation, RobotOrientations endOrientaion, Type stateType)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(StandardDimension)).Execute();
        var robot = new CreateRobotAction(new CreateRobotMessage(new Vector(location, initialOrientation))).Execute();
        new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(robot, planet)).Execute();

        new RotateRobotLeftAction(new RotateRobotLeftMessage(robot)).Execute();

        Assert.Multiple(() =>
        {
            Assert.That(robot.Location, Is.EqualTo(location));
            Assert.That(robot.Orientation, Is.EqualTo(endOrientaion));
            Assert.That(robot.RobotController.StateType, Is.EqualTo(stateType));
        });
    }

    [TestCaseSource(nameof(ForwardMovementsUseCases))]
    public void AUserCanMoveARobotForward(Location initialLocation, RobotOrientations orientation, Location endLocation, Type stateType)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(StandardDimension)).Execute();
        var robot = new CreateRobotAction(new CreateRobotMessage(new Vector(initialLocation, orientation))).Execute();
        new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(robot, planet)).Execute();

        new MoveRobotForwardAction(new MoveRobotForwardMessage(robot)).Execute();

        Assert.Multiple(() =>
        {
            Assert.That(robot.Location, Is.EqualTo(endLocation));
            Assert.That(robot.Orientation, Is.EqualTo(orientation));
            Assert.That(robot.RobotController.StateType, Is.EqualTo(stateType));
        });
    }

    [TestCaseSource(nameof(MoveARobotForwardOffAndEdgeUseCases))]
    public void AUserCanMoveARobotForwardOffAndEdge(RobotOrientations orientation)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(ZeroDimension)).Execute();
        var robot = new CreateRobotAction(new CreateRobotMessage(new Vector(ZeroLocation, orientation))).Execute();
        new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(robot, planet)).Execute();

        new MoveRobotForwardAction(new MoveRobotForwardMessage(robot)).Execute();

        var lost = robot.Lost;

        Assert.That(lost, Is.True);
    }

    [TestCaseSource(nameof(MoveARobotForwardOffAndEdgeUseCases))]
    public void AUserCanReleaseARobotOntoAPlanet(RobotOrientations orientation)
    {
        var planet = new CreatePlanetAction(new CreatePlanetMessage(StandardDimension)).Execute();
        var robot = new CreateRobotAction(new CreateRobotMessage(new Vector(ZeroLocation, orientation))).Execute();
        new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(robot, planet)).Execute();

        Assert.Multiple(() =>
        {
            Assert.That(robot.Orientation, Is.EqualTo(orientation));
        });


    }


}