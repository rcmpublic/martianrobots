
using MartianRobots.Core.Model;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class MarsParserTests
{
    [Test]
    public void AMarsParserTestsCanBeCreated()
    {
        new MarsParser(new DimensionParser());
    }

    [TestCase("1 2", 1, 2)]
    [TestCase("5 3", 5, 3)]
    public void AMarsPlanetCanBeParsed(string input, int sizeX, int sizeY)
    {
        var parser = new MarsParser(new DimensionParser());
        //var input="1 2";
        var mars = parser.Parse(input);

        var expectedDimension = new Dimension() { SizeX = sizeX, SizeY = sizeY };

        Assert.That(mars.Dimension, Is.EqualTo(expectedDimension));
    }

}
