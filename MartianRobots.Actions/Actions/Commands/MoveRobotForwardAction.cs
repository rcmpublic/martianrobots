using MartianRobots.Core.Actions.Commands;

namespace MartianRobots.Actions.Actions.Commands;

public class MoveRobotForwardAction : IRobotCommandAction
{
    private MoveRobotForwardMessage Message { get; set; }

    public MoveRobotForwardAction(MoveRobotForwardMessage message)
    {
        Message = message;
    }

    public void Execute()
    {
        Message.Robot.RobotController.MoveRobotForward();
    }
}