using System.Linq;
using System.Collections.Generic;
using MartianRobots.Core.Model;
using MartianRobots.Core.Persistence;

namespace MartianRobots.Persistence;

public class ScentsRepository : IScentsRepository
{
    private List<Scent> scents = new List<Scent>();
    public void Add(Scent scent)
    {
        scents.Add(scent);
    }
    public bool Exists(Scent scent)
    {
        return scents.FirstOrDefault(x => x.Equals(scent)) != null;
    }
}