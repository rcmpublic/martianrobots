using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Persistence;

namespace MartianRobotsTests;

[TestFixture]
public class ScentsTests
{
    [Test]
    public void ScentsCanBeCreated()
    {
        var repo = new ScentsRepository();
    }

    [Test]
    public void AScentCanBeAdded()
    {
        var repo = new ScentsRepository();
        repo.Add(new Scent(new Location(), RobotOrientations.North));
    }

    [Test]
    public void AScentCanBeFound()
    {
        var repo = new ScentsRepository();
        var added = new Scent(new Location(), RobotOrientations.North);
        repo.Add(added);

        var found = repo.Exists(added);

        Assert.That(found, Is.True);
    }
}