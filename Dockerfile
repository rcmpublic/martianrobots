FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /martianrobots

COPY . .

RUN dotnet restore

RUN dotnet build -c Release

WORKDIR /martianrobots/publish

RUN dotnet publish /martianrobots/MartianRobots/MartianRobots.csproj -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime

VOLUME /martianrobots_host

WORKDIR /martianrobots

COPY ./script.sh .

RUN chmod +x script.sh


COPY --from=build /martianrobots/publish/out .

ENTRYPOINT ["/martianrobots/script.sh"]