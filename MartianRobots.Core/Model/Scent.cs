using MartianRobots.Core.Model.Robot;
using System;

namespace MartianRobots.Core.Model;

public class Scent
{
    public Location Location { get; private set; }
    public RobotOrientations Orientation { get; private set; }
    public Scent(Location location, RobotOrientations orientation)
    {
        Location = location;
        Orientation = orientation;

    }
    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Scent other = (Scent)obj;
        return Location.Equals(other.Location) && Orientation.Equals(other.Orientation);
    }

    public override int GetHashCode()
    {
        return Tuple.Create(Location, Orientation).GetHashCode();
    }
}