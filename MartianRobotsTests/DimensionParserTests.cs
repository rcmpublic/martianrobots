
using MartianRobots.Core.Model;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class DimensionParserTests
{
    [Test]
    public void ADimensionParserCanBeCreated()
    {
        new DimensionParser();
    }

    [TestCase("1 2", 1, 2)]
    [TestCase("5 3", 5, 3)]
    public void ADimensionParserCanBeParsed(string input, int sizeX, int sizeY)
    {
        var parser = new DimensionParser();
        var dimension = parser.Parse(input);

        var expectedDimension = new Dimension() { SizeX = sizeX, SizeY = sizeY };

        Assert.That(dimension, Is.EqualTo(expectedDimension));
    }

}
