namespace MartianRobots.Core.Model;

public class Mars
{
    public Dimension Dimension { get; internal set; }
    public Mars(Dimension dimension)
    {
        Dimension = dimension;
    }

    public bool Contains(Location location)
    {
        return Dimension.Contains(location);
    }
}