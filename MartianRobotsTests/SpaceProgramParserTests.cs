using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class SpaceProgramParserTests
{
    [Test]
    public void ASpaceProgramParserCanBeCreated()
    {
        new SpaceProgramParser(new StringReader(""), null, null, null);
    }

    [Test]
    public void ASpaceProgramParserCanParseAPlanetDimension()
    {
        string input = "1 1";
        StringReader reader = new StringReader(input);
        var dimensionParser = new DimensionParser();

        var parser = new SpaceProgramParser(reader, dimensionParser, null, null);
        var dimension = parser.ParsePlanetDimension();

        Assert.IsNotNull(dimension);
    }

    [Test]
    public void ASpaceProgramParserCanParseAVector()
    {
        string input = "1 1 E";
        StringReader reader = new StringReader(input);
        var robotVectorParser = new RobotVectorParser(new LocationParser(), new RobotOrientationsParser());

        var parser = new SpaceProgramParser(reader, null, robotVectorParser, null);
        var robot = parser.ParseRobotVector();

        Assert.IsNotNull(robot);
        Assert.That(robot.Location, Is.EqualTo(new Location() { X = 1, Y = 1 }));
        Assert.That(robot.Orientation, Is.EqualTo(RobotOrientations.East));
    }

    [Test]
    public void ASpaceProgramParserCanParseRobotCommands()
    {
        string input = "RFRFRFRF";
        int commandsCount = input.Length;
        StringReader reader = new StringReader(input);

        var robotCommandSequenceParser = new RobotCommandsSequenceParser(new RobotCommandsParser());

        var parser = new SpaceProgramParser(reader, null, null, robotCommandSequenceParser);
        var commands = parser.ParseRobotCommands().ToList();

        Assert.IsNotNull(commands);
        int countRobotCommandAction = commands.OfType<RobotCommands>().Count();        
        Assert.That(countRobotCommandAction, Is.EqualTo(commandsCount));
    }


}
