using System.Linq;
using System.Collections.Generic;
using MartianRobots.Core.Model;

namespace MartianRobots.Core.Persistence;

public interface IScentsRepository
{
    public void Add(Scent scent);
    public bool Exists(Scent scent);
}