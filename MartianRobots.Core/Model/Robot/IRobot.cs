using System;
using System.ComponentModel.Design.Serialization;

namespace MartianRobots.Core.Model.Robot;
public interface IRobot
{
    public Location Location { get; }

    public bool Lost { get; }

    public RobotOrientations Orientation { get; }

    public IRobotController RobotController { get; }

}
