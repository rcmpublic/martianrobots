using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Robot.RobotStates;
public class EastState : IRobotState
{
    private readonly static Offset _forwardOffset = new Offset() { OffsetX = 1, OffsetY = 0 };
    private readonly RobotController _robotController;
    public RobotOrientations Orientation => RobotOrientations.East;
    public Offset ForwardOffset => _forwardOffset;

    public EastState(RobotController robotController)
    {
        _robotController = robotController;
    }

    public void RotateLeft()
    {
        _robotController.SetState(_robotController.NorthState);
    }

    public void RotateRight()
    {
        _robotController.SetState(_robotController.SouthState);
    }
}