using System.IO;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Core.IO;

public interface ISpaceProgramOutputWriter
{
    public void WriteRobot(IRobot robot);
}
