using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Core.Model;

public class Vector
{
    public Location Location { get; set; }

    public RobotOrientations Orientation { get; set; }

    public Vector(Location location, RobotOrientations orientation)
    {
        Location = location;
        Orientation = orientation;
    }
}
