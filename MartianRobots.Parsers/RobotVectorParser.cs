using System;
using System.Text.RegularExpressions;
using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class RobotVectorParser : IVectorParser
{
    private ILocationParser LocationParser { get; set; }
    private IRobotOrientationsParser RobotOrientationsParser { get; set; }
    private readonly string LocationGroup;
    private const string OneBlankSpace = @"\s";
    private readonly string RobotOrientantionsGroup;
    public string Pattern => @$"{LocationGroup}{OneBlankSpace}{RobotOrientantionsGroup}";

    public RobotVectorParser(ILocationParser locationParser, IRobotOrientationsParser robotOrientationsParser)
    {
        LocationParser = locationParser;
        RobotOrientationsParser = robotOrientationsParser;

        LocationGroup = @$"(?<{nameof(LocationGroup)}>{LocationParser.Pattern})";
        RobotOrientantionsGroup = @$"(?<{nameof(RobotOrientantionsGroup)}>{RobotOrientationsParser.Pattern})";
    }
    public Vector Parse(string input)
    {
        Match match = Regex.Match(input, Pattern);

        string locationInput = match.Groups[nameof(LocationGroup)].Value;
        Location location = LocationParser.Parse(locationInput);

        string robotOrientantionsInput = match.Groups[nameof(RobotOrientantionsGroup)].Value;
        RobotOrientations orientation = RobotOrientationsParser.Parse(robotOrientantionsInput);

        var vector = new Vector(location, orientation);

        return vector;
    }
}
