using System.Text.RegularExpressions;
using MartianRobots.Core.Model;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class MarsParser : IMarsParser
{
    private readonly IDimensionParser _dimensionParser;
    public MarsParser(IDimensionParser dimensionParser)
    {
        _dimensionParser = dimensionParser;
    }

    public Mars Parse(string input)
    {
        Dimension d = _dimensionParser.Parse(input);

        return new Mars(d);
    }
}
