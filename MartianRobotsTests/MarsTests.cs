using MartianRobots.Actions.Actions.Queries;
using MartianRobots.Core.Model;

namespace MartianRobotsTests;
[TestFixture]
public class MarsTests
{
    private static Dimension ZeroDimension { get { return new() { SizeX = 0, SizeY = 0 }; } }

    private static IEnumerable<TestCaseData> MarsContainsALocationTests
    {
        get
        {
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                {
                    var contained = (i == 0) && (j == 0);
                    var name = $"{nameof(Mars)} ({ZeroDimension.SizeX},{ZeroDimension.SizeY}) contains {nameof(Location)}({i},{j})";
                    yield return new TestCaseData(ZeroDimension, new Location() { X = i, Y = j }, contained).SetName(name);
                }
        }
    }

    [Test, TestCaseSource(nameof(MarsContainsALocationTests))]
    public void MarsContainsALocation(Dimension dimension, Location location, bool contained)
    {
        var mars = new CreatePlanetAction(new CreatePlanetMessage(dimension)).Execute();

        var result = mars.Contains(location);

        Assert.That(result, Is.EqualTo(contained));
    }
}
