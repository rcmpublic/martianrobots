using MartianRobots.Core.Model.Robot;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class OrientationParserTests
{
     private static IEnumerable<TestCaseData> ParseOrientationTests
    {
        get
        {
            yield return new TestCaseData("N",RobotOrientations.North);
            yield return new TestCaseData("S",RobotOrientations.South);
            yield return new TestCaseData("E",RobotOrientations.East);
            yield return new TestCaseData("W",RobotOrientations.West);
        }
    }

   [Test]
    public void AOrientationParserCanBeCreated()
    {
        new RobotOrientationsParser();
    }

    [TestCaseSource(nameof(ParseOrientationTests))]
    public void AOrientationCanBeParsed(string input, RobotOrientations expectedOrientation)
    {
        var parser = new RobotOrientationsParser();
        var orientation = parser.Parse(input);

        Assert.That(orientation, Is.EqualTo(expectedOrientation));
    }
}