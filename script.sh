#!/bin/bash

dotnet MartianRobots.dll --in /martianrobots_host/space_program_input.txt --out /martianrobots_host/space_program_output.txt
echo "Acceptance test executed"

mkdir -p /martianrobots_host/MartianRobots.Artifacts
cp -rf * /martianrobots_host/MartianRobots.Artifacts
echo "Artifacts copied to MartianRobots.Artifacts"

