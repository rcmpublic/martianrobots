using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Robot.RobotStates;
public class SouthState : IRobotState
{
    private readonly static Offset _forwardOffset = new Offset() { OffsetX = 0, OffsetY = -1 };
    private readonly RobotController _robotController;
    public RobotOrientations Orientation => RobotOrientations.South;
    public Offset ForwardOffset => _forwardOffset;
    public SouthState(RobotController robotController)
    {
        _robotController = robotController;
    }

    public void RotateLeft()
    {
        _robotController.SetState(_robotController.EastState);
    }

    public void RotateRight()
    {
        _robotController.SetState(_robotController.WestState);
    }
}