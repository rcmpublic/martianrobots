using System.Collections.Generic;
using System.Text.RegularExpressions;
using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class RobotCommandsSequenceParser : IRobotCommandsSequenceParser
{
    private IRobotCommandsParser Parser { get; set; }

    public RobotCommandsSequenceParser(IRobotCommandsParser parser)
    {
        Parser = parser;
    }
    public IEnumerable<RobotCommands> Parse(string sequence)
    {
        foreach (char c in sequence)
        {
            yield return Parser.Parse($"{c}");
        }
    }
}
