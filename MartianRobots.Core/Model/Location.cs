using System;

namespace MartianRobots.Core.Model;

public class Location
{
    public readonly static Location NullLocation = new Location { X = int.MinValue, Y = int.MinValue };
    public int X { get; set; }
    public int Y { get; set; }

    public Location Offset(Offset offset)
    {
        return new Location()
        {
            X = X + offset.OffsetX,
            Y = Y + offset.OffsetY
        };
    }
    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Location other = (Location)obj;

        return X.Equals(other.X) && Y.Equals(other.Y);
    }

    public override int GetHashCode()
    {
        return Tuple.Create(X, Y).GetHashCode();
    }

    public override string ToString()
    {
        return $"({X} {Y})";
    }
}