using System;
using System.Text.RegularExpressions;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Core.Parsers;

public interface IRobotOrientationsParser
{
    public string Pattern { get; }
    public RobotOrientations Parse(string input);

}
