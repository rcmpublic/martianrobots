using System.Text.RegularExpressions;
using MartianRobots.Core.Model;

namespace MartianRobots.Core.Parsers;

public interface IDimensionParser
{
    public Dimension Parse(string input);
}
