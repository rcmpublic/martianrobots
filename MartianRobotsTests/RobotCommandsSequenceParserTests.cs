using MartianRobots.Core.Actions.Commands;
using MartianRobots.Parsers;
using MartianRobots.Robot;

namespace MartianRobotsTests;


[TestFixture]
public class RobotCommandsParserParserTests
{
    private static IEnumerable<object[]> ARobotCommandsSequenceCanBeParsedTests
    {
        get
        {
            yield return new object[]{new string[]{"L","F","R"}};
        }
    }
    
    
    [Test]
    public void ARobotCommandsParserSequenceCanBeCreated()
    {
        var parser = new RobotCommandsSequenceParser(new RobotCommandsParser());
    }

    [TestCaseSource(nameof(ARobotCommandsSequenceCanBeParsedTests))]
    public void ARobotCommandsParserCanBeParsed( string[] commandsStrings)
    {
        var commands = new Dictionary<string, RobotCommands>
        {
            {"F" , RobotCommands.Forward},
            {"L" , RobotCommands.Left},
            {"R" , RobotCommands.Right}
        };
        
        var robot = new Robot(null,null);
        var parser = new RobotCommandsSequenceParser(new RobotCommandsParser());

        var commandsSequence = string.Join("",commandsStrings);
        var parsedCommands = parser.Parse(commandsSequence).ToArray();

        Assert.Multiple(() =>
        {
            Assert.That(parsedCommands.Length, Is.EqualTo(commandsStrings.Length));
            for (int i = 0; i< commandsStrings.Length; i++)
                Assert.That(parsedCommands[i], Is.EqualTo(commands[commandsStrings[i]]));
        });
    }

}
