using MartianRobots.Core.Actions.Commands;

namespace MartianRobots.Actions.Actions.Commands;

public class RotateRobotRightAction : IRobotCommandAction
{
    private RotateRobotRightMessage Message { get; set; }
    public RotateRobotRightAction(RotateRobotRightMessage message)
    {
        Message = message;
    }
    public void Execute()
    {
        Message.Robot.RobotController.RotateRight();
    }
}