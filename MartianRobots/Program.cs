﻿using System;
using System.IO;
using System.CommandLine;
using MartianRobots.Application;
using MartianRobots.Parsers;
using MartianRobots.Persistence;
using MartianRobots.IO;

namespace MartianRobots
{
    class Program
    {
        static void Main(string[] args)
        {
            var rootCommand = new RootCommand
            {
                Description = "Martian Robots",
            };

            var spaceProgramInputFileOption = new Option<FileInfo>(
                "--in",
                "Specify a file to read input from (default space_program_input.txt) "
            );

            rootCommand.AddOption(spaceProgramInputFileOption);
            

            var spaceProgramOutputFileOption = new Option<FileInfo>(
                "--out",
                "Specify a file to write output (default space_program_output.txt)"
            );            

            rootCommand.AddOption(spaceProgramOutputFileOption);

            rootCommand.SetHandler(
                (fileInfoIn, fileInfoOut) => {
                        fileInfoIn ??= new FileInfo("space_program_input.txt");

                        if(fileInfoIn?.Exists == false)
                        {
                           Console.WriteLine($"{fileInfoIn?.FullName} not found."); 
                           return;
                        }

                        TextReader reader = new StreamReader(fileInfoIn.FullName);
                        TextWriter writer = (fileInfoOut != null) ? new StreamWriter(fileInfoOut.FullName): Console.Out;

                        using (var spaceProgramReader = reader)
                        using (var spaceProgramWriter = writer)
                            ExecuteSpaceProgram(spaceProgramReader, spaceProgramWriter);


                     },
                spaceProgramInputFileOption,
                spaceProgramOutputFileOption); 

            
            rootCommand.Invoke(args);           
        }

        private static void ExecuteSpaceProgram(TextReader spaceProgramReader, TextWriter spaceProgramWriter)
        {
            var robotVectorParser = new RobotVectorParser(new LocationParser(), new RobotOrientationsParser());
            var dimensionParser = new DimensionParser();
            var robotCommandsSequenceParser = new RobotCommandsSequenceParser(new RobotCommandsParser());
            var spaceProgramParser = new SpaceProgramParser(spaceProgramReader, dimensionParser, robotVectorParser, robotCommandsSequenceParser);
            var spaceProgramOutputWriter = new SpaceProgramOutputWriter(spaceProgramWriter);
            var scentsRepository = new ScentsRepository();
            new SpaceProgramExecutor(spaceProgramParser,spaceProgramOutputWriter, scentsRepository).Execute();
        }
    }
}
