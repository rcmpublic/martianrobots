using System;

namespace MartianRobots.Core.Model.Robot;

public class RobotOrientations
{
    public static readonly RobotOrientations NullOrientation = new RobotOrientations(string.Empty, nameof(NullOrientation));
    public static readonly RobotOrientations North = new RobotOrientations("N", nameof(North));
    public static readonly RobotOrientations South = new RobotOrientations("S", nameof(South));
    public static readonly RobotOrientations East = new RobotOrientations("E", nameof(East));
    public static readonly RobotOrientations West = new RobotOrientations("W", nameof(West));

    public string Orientation { get; private set; }
    public string Description { get; private set; }

    public RobotOrientations(string orientation, string description)
    {
        Orientation = orientation;
        Description = description;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        RobotOrientations other = (RobotOrientations)obj;
        return Orientation == other.Orientation && Description == other.Description;
    }

    public override int GetHashCode()
    {
        return Tuple.Create(Orientation, Description).GetHashCode();
    }

    public override string ToString()
    {
        return Orientation;
    }
}