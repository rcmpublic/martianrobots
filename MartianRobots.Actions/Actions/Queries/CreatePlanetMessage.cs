using MartianRobots.Core.Model;

namespace MartianRobots.Actions.Actions.Queries;

public class CreatePlanetMessage
{
    public Dimension Dimension { get; private set; }
    public CreatePlanetMessage(Dimension dimension)
    {
        Dimension = dimension;
    }
}