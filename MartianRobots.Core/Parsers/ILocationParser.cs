using System.Text.RegularExpressions;
using MartianRobots.Core.Model;

namespace MartianRobots.Core.Parsers;

public interface ILocationParser
{
    public string Pattern { get; }
    public Location Parse(string input);
}
