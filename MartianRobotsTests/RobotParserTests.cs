
using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class RobotParserTests
{
    private static IEnumerable<TestCaseData> ParseRobotUseCases
    {
        get
        {
            yield return new TestCaseData("1 1 E", 1, 1, RobotOrientations.East);
            yield return new TestCaseData("3 2 N", 3, 2, RobotOrientations.North);
            yield return new TestCaseData("0 3 W", 0, 3, RobotOrientations.West);
            yield return new TestCaseData("0 3 S", 0, 3, RobotOrientations.South);
        }
    }


    [Test]
    public void ARobotVectorParserTestsCanBeCreated()
    {
        new RobotVectorParser(new LocationParser(), new RobotOrientationsParser());
    }

    [TestCaseSource(nameof(ParseRobotUseCases))]
    public void ARobotVectorCanBeParsed(string input, int x, int y, RobotOrientations orientation)
    {
        var parser = new RobotVectorParser(new LocationParser(), new RobotOrientationsParser());
        var expectedLocation = new Location() { X = x, Y = y };
        var robot = parser.Parse(input);

        Assert.That(robot.Location, Is.EqualTo(expectedLocation));
        Assert.That(robot.Orientation, Is.EqualTo(orientation));
    }
}
