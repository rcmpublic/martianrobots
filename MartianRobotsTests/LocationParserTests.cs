using MartianRobots.Core.Model;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class LocationParserTests
{
    [Test]
    public void ALocationParserCanBeCreated()
    {
        new LocationParser();
    }

    [TestCase("1 2", 1, 2)]
    [TestCase("5 3", 5, 3)]
    public void ALocationCanBeParsed(string input, int x, int y)
    {
        var parser = new LocationParser();
        var location = parser.Parse(input);

        var expectedLocation = new Location() { X = x, Y = y };

        Assert.That(location, Is.EqualTo(expectedLocation));
    }
}