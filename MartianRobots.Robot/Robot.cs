using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using System;
using System.ComponentModel.Design.Serialization;

namespace MartianRobots.Robot;
public class Robot : IRobot
{
    private const string LostDescription = "LOST";
    public Location Location { get; internal set; }

    public bool Lost { get; internal set; }

    public RobotOrientations Orientation { get; internal set; }

    public IRobotController RobotController { get; set; }

    public Robot(Location location, RobotOrientations orientation)
    {
        Location = location;
        Orientation = orientation;
    }

    public override string ToString()
    {
        string lostString = Lost ? $" {LostDescription}" : string.Empty;
        return $"{Location.X} {Location.Y} {Orientation}{lostString}";
    }
}
