using System.Collections.Generic;
using System.Text.RegularExpressions;
using MartianRobots.Core.Actions.Commands;

namespace MartianRobots.Core.Parsers;

public interface IRobotCommandsSequenceParser
{
    public IEnumerable<RobotCommands> Parse(string sequence);
}
