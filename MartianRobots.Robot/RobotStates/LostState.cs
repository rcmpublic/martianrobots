using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Robot.RobotStates;
public class LostState : IRobotState
{
    private readonly static Offset _forwardOffset = new Offset() { OffsetX = 0, OffsetY = 0 };
    private readonly RobotController _robotController;
    public RobotOrientations Orientation => _robotController.Robot.Orientation;
    public Offset ForwardOffset => _forwardOffset;
    public LostState(RobotController robotController)
    {
        _robotController = robotController;
    }
    public void RotateLeft()
    {
        // void on purpose
    }

    public void RotateRight()
    {
        // void on purpose
    }
}
