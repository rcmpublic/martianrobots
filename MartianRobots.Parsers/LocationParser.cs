using System.Text.RegularExpressions;
using MartianRobots.Core.Model;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class LocationParser : ILocationParser
{
    private const string OneOrMoreDigits = @"(\d+)";
    private const string OneBlankSpace = @"\s";
    public string Pattern => @$"{OneOrMoreDigits}{OneBlankSpace}{OneOrMoreDigits}";
    public Location Parse(string input)
    {
        Match match = Regex.Match(input, Pattern);

        int x = int.Parse(match.Groups[1].Value);
        int y = int.Parse(match.Groups[2].Value);

        return new Location() { X = x, Y = y };

    }
}
