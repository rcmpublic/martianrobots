using System;

namespace MartianRobots.Core.Model;

public class Offset
{
    public int OffsetX { get; set; }
    public int OffsetY { get; set; }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Offset other = (Offset)obj;

        return OffsetX.Equals(other.OffsetX) && OffsetY.Equals(other.OffsetY);
    }

    public override int GetHashCode()
    {
        return Tuple.Create(OffsetX, OffsetY).GetHashCode();
    }

}