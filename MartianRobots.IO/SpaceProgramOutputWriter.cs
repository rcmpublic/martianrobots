using System.IO;
using MartianRobots.Core.IO;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.IO;

public class SpaceProgramOutputWriter : ISpaceProgramOutputWriter
{
    public TextWriter Writer { get; set; }
    public SpaceProgramOutputWriter(TextWriter writer)
    {
        Writer = writer;
    }

    public void WriteRobot(IRobot robot)
    {
        Writer.WriteLine(robot);
    }

}
