
namespace MartianRobots.Actions.Actions.Commands;
public class RotateRobotLeftMessage : RobotCommandActionMessage
{
    public RotateRobotLeftMessage(Robot.Robot robot) : base(robot)
    {
    }
}