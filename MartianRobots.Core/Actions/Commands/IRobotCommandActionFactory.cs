using System;

namespace MartianRobots.Core.Actions.Commands;
public interface IRobotCommandActionFactory
{
    public IRobotCommandAction Create(RobotCommands command);
}