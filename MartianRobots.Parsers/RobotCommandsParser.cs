using System.Collections.Generic;
using System.Text.RegularExpressions;
using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Parsers;

namespace MartianRobots.Parsers;

public class RobotCommandsParser : IRobotCommandsParser
{
    private Dictionary<string, RobotCommands> Commands { get; set; }
    public static string Pattern => @$"([{RobotCommands.Forward}{RobotCommands.Left}{RobotCommands.Right}])";

    public RobotCommandsParser()
    {
        Commands = new Dictionary<string, RobotCommands>()
        {
            {"F" , RobotCommands.Forward},
            {"L" , RobotCommands.Left},
            {"R" , RobotCommands.Right}
        };

    }
    public RobotCommands Parse(string input)
    {
        Match match = Regex.Match(input, Pattern);
        return Commands[match.Groups[1].Value];
    }
}

