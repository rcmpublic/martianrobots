
namespace MartianRobots.Actions.Actions.Commands;
public class RobotCommandActionMessage
{
    public Robot.Robot Robot { get; protected set; }
    public RobotCommandActionMessage(Robot.Robot robot)
    {
        Robot = robot;
    }
}