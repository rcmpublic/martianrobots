namespace MartianRobots.Actions.Actions.Commands;
public class MoveRobotForwardMessage : RobotCommandActionMessage
{
    public MoveRobotForwardMessage(Robot.Robot robot) : base(robot)
    {
    }
}