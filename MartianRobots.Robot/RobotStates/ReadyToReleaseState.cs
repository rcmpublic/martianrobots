using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;
using MartianRobots.Robot;

namespace MartianRobots.Robot.RobotStates;

public class ReadyToReleaseState : IRobotState
{
    private readonly static Offset _forwardOffset = new Offset() { OffsetX = 0, OffsetY = 0 };
    private readonly RobotController _robotController;
    public RobotOrientations Orientation => RobotOrientations.NullOrientation;
    public Offset ForwardOffset => _forwardOffset;

    public ReadyToReleaseState(RobotController robotController)
    {
        _robotController = robotController;
    }

    public void ReleaseOntoPlanet()
    {

    }
    public void RotateLeft()
    {

    }

    public void RotateRight()
    {
    }
}
