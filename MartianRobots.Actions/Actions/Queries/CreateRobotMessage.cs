using MartianRobots.Core.Model;

namespace MartianRobots.Actions.Actions.Queries;

public class CreateRobotMessage
{
    public Vector Vector { get; }
    public CreateRobotMessage(Vector vector)
    {
        Vector = vector;
    }
}
