using System;

namespace MartianRobots.Core.Model;
public class Dimension
{
    public int SizeX { get; set; }
    public int SizeY { get; set; }

    public bool Contains(Location location)
    {
        var insideAxisX = 0 <= location.X && location.X <= SizeX;
        var insideAxisY = 0 <= location.Y && location.Y <= SizeY;
        return insideAxisX && insideAxisY;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Dimension other = (Dimension)obj;

        return SizeX.Equals(other.SizeX) && SizeY.Equals(other.SizeY);
    }

    public override int GetHashCode()
    {
        return Tuple.Create(SizeX, SizeY).GetHashCode();
    }

    public override string ToString()
    {
        return $"({SizeX} {SizeY})";
    }
}