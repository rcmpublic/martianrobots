using MartianRobots.Core.Actions.Commands;

namespace MartianRobots.Actions.Actions.Commands;

public class ReleaseARobotOntoAPlanetAction : IRobotCommandAction
{
    private ReleaseARobotOntoAPlanetMessage Message { get; set; }

    public ReleaseARobotOntoAPlanetAction(ReleaseARobotOntoAPlanetMessage message)
    {
        Message = message;
    }

    public void Execute()
    {
        Message.Robot.RobotController.ReleaseOntoPlanet(Message.Planet);
    }
}