# .NET - Martian Robots

## Introduction

This repository implements the control of the movements of Robots over Mars planet, which is modelled as a rectangular grid surface.

## Some notes regarding to the implementation

It seems that this projects is implemented out there as a TDD Kata. I have done some Katas before,and was put off by the idea of looking for solutions provided over there. I wanted to do it by myself, as i thought this could lead me to fail, practice un uppon all learn.

I have recently seen different ways to do TDD: Classic (Inside-Out/Chicago) and Mockist (Outside-In/London) TDD and [IDD](https://www.codurance.com/es/publications/introduccion-al-idd). I actually tried to follow this approach with this project.

But the output i was most interested in was the Architecture. I wanted to follow Uncle Bob Clean Architecture, using Inversion of Dependence (and the other SOLID principles together with TDD rule TDD Red-Green-Refactor) to check that metrics regarding to Abstracness and Instability of the packages were acceptable.

Along this path appeared Design Patterns such as: State, Command/Query, Factory, Mediator, Repository, Null Pattern.

As always, It is interesting how development evolves with TDD and refactors. The requirements would have invite me to think that the problem might be solve it using arrays. But us Uncle Bob states, these are "details".

Deferring "Details" is why, even the requirements states that none of the coordinates should be higher than 50 and all instructions shorter than 100 characters, the current implementation doesn't have those restrictions.

But one could unplug and plug in a new implementation of the current classes to take into account those restrictions if they are so important.

That's another interesting point, the cost of change. Sure the architecture can be improved, but as I was refactoring I realised that it wouldn't cost much to change the implementation for several ideas that came to my mind such as : Mars modelled as a three-dimensional cube, support for different syntaxes in input and output, multi-language input and output, other orientations ("NW", "NE", "SE"...), move backwards...etc.

Creating an API, or persisting robot trajectory data in a sqlite database, or similar are (not to diminish their value or importance) "details" that I didn't want to focus on, because, as I said, it took me more time to practice on Architecture, TDD and SOLID.

However, it is still a project that can evolve to improve further and to which I will be able to return to see what could be improved.

## Architecture

As i said before, i followed Clean Architecture. Below I show a series of graphs related to the architecture and metrics of the application.

### Packages Dependency Graph

As you can see Tests package is the project with more dependencies. I usually create one test suite per code package. On this occasion, for simplicity, I have created a single package for all tests.

![1694699681534](MartianRobots.Docs/README/packages.png)

### Dependency Matrix

![1694700544647](MartianRobots.Docs/README/1694700544647.png)

### Abstractness versus Instability

As you can see the only package that has a lot of room for improvement is the MartianRobots.Robots package. The rest are in the green zone or almost in the green zone.

![1694700719350](MartianRobots.Docs/README/1694700719350.png)

## Tools and development environment

### Visual Studio Code and Docker Dev Containers

I have created my own C# development environment, where I can build and debug unit tests for events. Git is also integrated in visual studio code. It's not as mature as Visual Studio, and it has its bugs, but this way anyone could start developing in minutes sharing the same development environment. I also separate all the artillery for development from the host machine.

I developed on a windows OS machine but the approach I followed should be compatible with any OS that can run visual studio code and the docker extension.

Below is a screenshot of visual studio code debugging a clockwise Robot rotation test.

![1694701750522](MartianRobots.Docs/README/1694701750522.png)

### NDepend

NDepend is a static analysis tool for .NET managed code tool was my guide to improve the distribution by packages. It is a very powerful tool from which a lot of information can be extracted. All the previous images where generated with this tool.

### Docker

Docker was not only useful to create the development environment but also to be able to run the code against an input file as I explain below.

### ChatGPT

Yes, ChatGPT also. It was helpful to solve some doubts, provide some examples regarding to design patterns. But nothing more (and nothing less).

## Running a Space Program

I felt that "Space Program" was an appropriate name for files containing the project input.

### Fastest Way : Build Artifacts And Run Acceptance From A Provided Script File

At the root of the code repository is the ***space_program_input.txt***. The content of this file is precisely the acceptance test :

```
5 3
1 1 E
RFRFRFRF
3 2 N
FRRFLLFFRRFLL
0 3 W
LLFFFRFLFL
```

Also at the root of the code repository there is a a batch script file (.sh for linux) named ***build_artifacts_and_run_acceptance_test.bat*** . If you run it (from windows explorer for example) then behind the scenes it will :

* Build a docker image that will publish the binaries inside the container.
* Execute Acceptance test against the ***space_program_input.txt*** . The output will be a file named ***space_program_output.txt*** at the root of your repository folder.
* Copy published artifacts at a folder named ***MartianRobots.Artifacts*** at the root of your repository folder.

The produced output will be ***space_program_output.txt***

```
1 1 E
3 3 N LOST
4 2 N
```

**If this succeeds then you will have more options :**

* Use the built **MartianRobots.Artifacts** to run more tests at your host machine (if dotnet 6.0 runtime is installed in your host machine)
* Edit ***space_program_input.txt*** as you prefer an run the script file again.

### From Visual Studio Code Docker Dev Environment

Remember to install Dev Containers extension from the Visual Studio Code Marketplace. Then Visual Studio Code will suggest you to open the workspace from inside the container. More info at [https://code.visualstudio.com/docs/devcontainers/containers]() .

## Command Line Help

To build the command line console help it was used the a microsoft nugget still in beta version ([https://www.nuget.org/packages/System.CommandLine]())

This is the output of the help command.

```
root@4ddde6c7541b:/martianrobots_host/MartianRobots/bin/debug/net6.0# dotnet MartianRobots.dll --help
Description:
  Martian Robots

Usage:
  MartianRobots [options]

Options:
  --in <in>       Specify a file to read input from (default space_program_input.txt)
  --out <out>     Specify a file to write output (default space_program_output.txt)
  --version       Show version information
  -?, -h, --help  Show help and usage information
```
