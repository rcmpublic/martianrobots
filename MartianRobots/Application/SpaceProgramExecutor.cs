using MartianRobots.Actions.Actions.Commands;
using MartianRobots.Actions.Actions.Queries;
using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.IO;
using MartianRobots.Core.Model;
using MartianRobots.Core.Parsers;
using MartianRobots.Core.Persistence;

namespace MartianRobots.Application;
public class SpaceProgramExecutor
{
    private ISpaceProgramParser SpaceProgramParser { get; set; }
    private ISpaceProgramOutputWriter SpaceProgramOutputWriter { get; set; }
    private IScentsRepository ScentsRepository { get; set; }
    public SpaceProgramExecutor(
        ISpaceProgramParser spaceProgramParser, 
        ISpaceProgramOutputWriter spaceProgramOutputWriter, 
        IScentsRepository scentsRepository)
    {
        SpaceProgramParser = spaceProgramParser;
        SpaceProgramOutputWriter = spaceProgramOutputWriter;
        ScentsRepository = scentsRepository;
    }

    public void Execute()
    {
        var dimension = SpaceProgramParser.ParsePlanetDimension();
        var mars = new CreatePlanetAction(new CreatePlanetMessage(dimension)).Execute();

        while(!SpaceProgramParser.EndReached)
        {
            var vector = SpaceProgramParser.ParseRobotVector();
            var robot = new CreateRobotAction(new CreateRobotMessage(vector)).Execute();
            var commandsFactory = new RobotCommandActionFactory(robot, mars);
            commandsFactory.Create(RobotCommands.ReleaseOntoPlanet).Execute();

            foreach(var parsedRobotCommand in SpaceProgramParser.ParseRobotCommands())
            {
                var scent = new Scent(robot.Location, robot.Orientation);
                var scentExists = ScentsRepository.Exists(scent);
                var forwardingToScent = scentExists && parsedRobotCommand == RobotCommands.Forward;

                if(!forwardingToScent)
                {         
                    var robotCommandAction = commandsFactory.Create(parsedRobotCommand);

                    robotCommandAction.Execute();

                    if(robot.Lost)
                        ScentsRepository.Add(new Scent(robot.Location, robot.Orientation));
                }
            }

            SpaceProgramOutputWriter.WriteRobot(robot);
        }
    }

}
