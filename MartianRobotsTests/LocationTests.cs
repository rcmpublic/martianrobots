using MartianRobots.Core.Model;

namespace MartianRobotsTests;

public class LocationTests
{
    [Test]
    public void ALocationCanBeCreated()
    {
        new Location();
    }

    [Test]
    public void AnOffsetLocationCanBeRequested()
    {
        var initialLocation = new Location();
        var offsetLocation = new Location().Offset(new Offset() { OffsetX = 1, OffsetY = 1 });
        var expectedLocation = new Location() { X = 1, Y = 1 };

        Assert.That(offsetLocation, Is.EqualTo(expectedLocation));
    }

}