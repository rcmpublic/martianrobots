using System.IO;
using System.Collections.Generic;
using MartianRobots.Core.Parsers;
using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Model;

namespace MartianRobots.Parsers;

public class SpaceProgramParser : ISpaceProgramParser
{
    private string _lastReadLine = null;
    public bool EndReached => _lastReadLine == null;
    private TextReader Reader { get; set; }
    private IDimensionParser DimensionParser { get; set; }
    private IVectorParser VectorParser { get; set; }
    private IRobotCommandsSequenceParser RobotCommandsSequenceParser { get; set; }

    public SpaceProgramParser(TextReader reader, IDimensionParser dimensionParser, IVectorParser vectorParser, IRobotCommandsSequenceParser robotCommandsSequenceParser)
    {
        Reader = reader;
        DimensionParser = dimensionParser;
        VectorParser = vectorParser;
        RobotCommandsSequenceParser = robotCommandsSequenceParser;
        _lastReadLine = reader.ReadLine();
    }
    public Dimension ParsePlanetDimension()
    {
        var dimension = DimensionParser.Parse(_lastReadLine);
        _lastReadLine = Reader.ReadLine();
        return dimension;
    }

    public Vector ParseRobotVector()
    {
        var vector = VectorParser.Parse(_lastReadLine);
        _lastReadLine = Reader.ReadLine();
        return vector;
    }
    public IEnumerable<RobotCommands> ParseRobotCommands()
    {
        var robotCommands = RobotCommandsSequenceParser.Parse(_lastReadLine);
        _lastReadLine = Reader.ReadLine();
        return robotCommands;
    }


}
