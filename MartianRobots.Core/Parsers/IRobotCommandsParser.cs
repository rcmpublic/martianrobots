using System.Collections.Generic;
using System.Text.RegularExpressions;
using MartianRobots.Core.Actions.Commands;

namespace MartianRobots.Core.Parsers;

public interface IRobotCommandsParser
{
    public RobotCommands Parse(string input);
}

