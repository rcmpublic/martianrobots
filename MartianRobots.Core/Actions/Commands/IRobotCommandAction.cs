namespace MartianRobots.Core.Actions.Commands;

public interface IRobotCommandAction
{
    public void Execute();
}