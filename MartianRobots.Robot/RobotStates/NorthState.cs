using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Robot.RobotStates;
public class NorthState : IRobotState
{
    private readonly static Offset _forwardOffset = new Offset() { OffsetX = 0, OffsetY = +1 };
    private readonly RobotController _robotController;
    public RobotOrientations Orientation => RobotOrientations.North;
    public Offset ForwardOffset => _forwardOffset;

    public NorthState(RobotController robotController)
    {
        _robotController = robotController;
    }
    public void RotateLeft()
    {
        _robotController.SetState(_robotController.WestState);
    }

    public void RotateRight()
    {
        _robotController.SetState(_robotController.EastState);
    }
}
