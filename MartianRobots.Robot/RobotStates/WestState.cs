using MartianRobots.Core.Model;
using MartianRobots.Core.Model.Robot;

namespace MartianRobots.Robot.RobotStates;
public class WestState : IRobotState
{
    private readonly static Offset _forwardOffset = new Offset() { OffsetX = -1, OffsetY = 0 };
    private readonly RobotController _robotController;
    public RobotOrientations Orientation => RobotOrientations.West;
    public Offset ForwardOffset => _forwardOffset;
    public WestState(RobotController robotController)
    {
        _robotController = robotController;
    }

    public void RotateLeft()
    {
        _robotController.SetState(_robotController.SouthState);
    }

    public void RotateRight()
    {
        _robotController.SetState(_robotController.NorthState);
    }
}
