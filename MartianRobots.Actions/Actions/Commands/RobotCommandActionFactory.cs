using System;
using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Model;

namespace MartianRobots.Actions.Actions.Commands;
public class RobotCommandActionFactory : IRobotCommandActionFactory
{
    public Robot.Robot Robot { get; protected set; }
    public Mars Mars { get; protected set; }
    public RobotCommandActionFactory(Robot.Robot robot, Mars mars)
    {
        Robot = robot;
        Mars = mars;
    }

    public IRobotCommandAction Create(RobotCommands command)
    {
        if (command == RobotCommands.ReleaseOntoPlanet)
            return new ReleaseARobotOntoAPlanetAction(new ReleaseARobotOntoAPlanetMessage(Robot, Mars));
        if (command == RobotCommands.Forward)
            return new MoveRobotForwardAction(new MoveRobotForwardMessage(Robot));
        else if (command == RobotCommands.Left)
            return new RotateRobotLeftAction(new RotateRobotLeftMessage(Robot));
        else if (command == RobotCommands.Right)
            return new RotateRobotRightAction(new RotateRobotRightMessage(Robot));
        else
            throw new InvalidOperationException($"Action for command {command} not supported");
    }
}