using MartianRobots.Core.Model;

namespace MartianRobots.Core.Model.Robot;
public interface IRobotState
{
    public RobotOrientations Orientation { get; }
    public Offset ForwardOffset { get; }
    public void RotateLeft();
    public void RotateRight();
}
