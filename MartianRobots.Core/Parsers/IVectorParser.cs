using System;
using System.Text.RegularExpressions;
using MartianRobots.Core.Model;

namespace MartianRobots.Core.Parsers;

public interface IVectorParser
{
    public Vector Parse(string input);
}
