using MartianRobots.Core.Actions.Commands;

namespace MartianRobots.Actions.Actions.Commands;

public class RotateRobotLeftAction : IRobotCommandAction
{

    private RotateRobotLeftMessage Message { get; set; }

    public RotateRobotLeftAction(RotateRobotLeftMessage message)
    {
        Message = message;
    }
    public void Execute()
    {
        Message.Robot.RobotController.RotateLeft();
    }
}