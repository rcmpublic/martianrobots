using MartianRobots.Core.Actions.Commands;
using MartianRobots.Core.Model;
using MartianRobots.Parsers;

namespace MartianRobotsTests;

[TestFixture]
public class RobotCommandParserTests
{
    private static Location OneLocation { get { return new() { X = +1, Y = +1 }; } }

    private static IEnumerable<TestCaseData> RobotCommandCanBeParsedTests
    {
        get
        {
            yield return new TestCaseData("F", RobotCommands.Forward);
            yield return new TestCaseData("L", RobotCommands.Left);
            yield return new TestCaseData("R", RobotCommands.Right);
        }
    }


    [Test]
    public void ARobotCommandParserCanBeCreated()
    {
        new RobotCommandsParser();
    }

    [TestCaseSource(nameof(RobotCommandCanBeParsedTests))]
    public void ARobotCommandCanBeParsed(string input,RobotCommands expectedCommand)
    {
        var parser = new RobotCommandsParser();
        var command = parser.Parse(input);

        Assert.That(command, Is.EqualTo(expectedCommand));
    }
}